#!/usr/bin/env python3

import argparse, json
import matplotlib.pyplot, matplotlib.animation


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("filename", type=str)
    args = parser.parse_args()

    with open(args.filename, "r") as f:
        ims_data = json.load(f)

    fig, ax = matplotlib.pyplot.subplots()

    ims = []

    for im_data in ims_data:
        if len(im_data) == 512:
            h = 8  # ?
            w = 64
        else:
            print(f"Don't understand data of length {len(im_data)}")
            exit()

        d = [im_data[w * i : w * (i + 1)] for i in range(h)]

        im = ax.imshow(d, cmap="gray", vmin=0, vmax=255, animated=True)

        ims.append([im])

    ani = matplotlib.animation.ArtistAnimation(
        fig, ims, interval=200, blit=True
    )

    matplotlib.pyplot.show()
